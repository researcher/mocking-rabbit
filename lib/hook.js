'use strict';

exports.__esModule = true;

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _events = require('events');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Consumer = function () {
  function Consumer(tracker) {
    (0, _classCallCheck3.default)(this, Consumer);

    this.tracker = tracker;
    this.consumers = [];
  }

  Consumer.prototype.reset = function reset() {
    this.consumers = [];
  };

  Consumer.prototype.track = function track(consumer, callback, resolve) {
    var step = void 0;

    if (this.tracker.tracking) {
      // eslint-disable-next-line no-param-reassign
      consumer.response = function response(result) {
        // eslint-disable-next-line no-param-reassign
        consumer.result = result;
        callback(result);
        resolve(result);
      };

      // eslint-disable-next-line no-param-reassign
      delete consumer.result;

      step = this.consumers.push(consumer);
      this.tracker.emit('consume', consumer, step);
    }
  };

  Consumer.prototype.first = function first() {
    return this.consumers[0];
  };

  Consumer.prototype.count = function count() {
    return this.consumers.length;
  };

  Consumer.prototype.last = function last() {
    return this.consumers[this.count() - 1];
  };

  Consumer.prototype.at = function at(step) {
    return this.consumers[step - 1];
  };

  return Consumer;
}();

var Tracker = function (_EventEmitter) {
  (0, _inherits3.default)(Tracker, _EventEmitter);

  function Tracker() {
    (0, _classCallCheck3.default)(this, Tracker);

    var _this = (0, _possibleConstructorReturn3.default)(this, _EventEmitter.call(this));

    _this.tracking = false;
    _this.consumer = new Consumer(_this);
    return _this;
  }

  Tracker.prototype.install = function install() {
    this.tracking = true;
    this.consumer.reset();
  };

  Tracker.prototype.uninstall = function uninstall() {
    this.tracking = false;
    this.consumer.reset();
    this.removeAllListeners('consume');
  };

  return Tracker;
}(_events.EventEmitter);

exports.default = { Tracker: Tracker, Consumer: Consumer, tracker: new Tracker() };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9ob29rLmpzIl0sIm5hbWVzIjpbIkNvbnN1bWVyIiwidHJhY2tlciIsImNvbnN1bWVycyIsInJlc2V0IiwidHJhY2siLCJjb25zdW1lciIsImNhbGxiYWNrIiwicmVzb2x2ZSIsInN0ZXAiLCJ0cmFja2luZyIsInJlc3BvbnNlIiwicmVzdWx0IiwicHVzaCIsImVtaXQiLCJmaXJzdCIsImNvdW50IiwibGVuZ3RoIiwibGFzdCIsImF0IiwiVHJhY2tlciIsImluc3RhbGwiLCJ1bmluc3RhbGwiLCJyZW1vdmVBbGxMaXN0ZW5lcnMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDQTs7OztJQUVNQSxRO0FBRUosb0JBQVlDLE9BQVosRUFBcUI7QUFBQTs7QUFDbkIsU0FBS0EsT0FBTCxHQUFlQSxPQUFmO0FBQ0EsU0FBS0MsU0FBTCxHQUFpQixFQUFqQjtBQUNEOztxQkFFREMsSyxvQkFBUTtBQUNOLFNBQUtELFNBQUwsR0FBaUIsRUFBakI7QUFDRCxHOztxQkFFREUsSyxrQkFBTUMsUSxFQUFVQyxRLEVBQVVDLE8sRUFBUztBQUNqQyxRQUFJQyxhQUFKOztBQUVBLFFBQUksS0FBS1AsT0FBTCxDQUFhUSxRQUFqQixFQUEyQjtBQUN6QjtBQUNBSixlQUFTSyxRQUFULEdBQW9CLFNBQVNBLFFBQVQsQ0FBa0JDLE1BQWxCLEVBQTBCO0FBQzVDO0FBQ0FOLGlCQUFTTSxNQUFULEdBQWtCQSxNQUFsQjtBQUNBTCxpQkFBU0ssTUFBVDtBQUNBSixnQkFBUUksTUFBUjtBQUNELE9BTEQ7O0FBT0E7QUFDQSxhQUFPTixTQUFTTSxNQUFoQjs7QUFFQUgsYUFBTyxLQUFLTixTQUFMLENBQWVVLElBQWYsQ0FBb0JQLFFBQXBCLENBQVA7QUFDQSxXQUFLSixPQUFMLENBQWFZLElBQWIsQ0FBa0IsU0FBbEIsRUFBNkJSLFFBQTdCLEVBQXVDRyxJQUF2QztBQUNEO0FBQ0YsRzs7cUJBRURNLEssb0JBQVE7QUFDTixXQUFPLEtBQUtaLFNBQUwsQ0FBZSxDQUFmLENBQVA7QUFDRCxHOztxQkFFRGEsSyxvQkFBUTtBQUNOLFdBQU8sS0FBS2IsU0FBTCxDQUFlYyxNQUF0QjtBQUNELEc7O3FCQUVEQyxJLG1CQUFPO0FBQ0wsV0FBTyxLQUFLZixTQUFMLENBQWUsS0FBS2EsS0FBTCxLQUFlLENBQTlCLENBQVA7QUFDRCxHOztxQkFFREcsRSxlQUFHVixJLEVBQU07QUFDUCxXQUFPLEtBQUtOLFNBQUwsQ0FBZU0sT0FBTyxDQUF0QixDQUFQO0FBQ0QsRzs7Ozs7SUFHR1csTzs7O0FBRUoscUJBQWM7QUFBQTs7QUFBQSwrREFDWix3QkFEWTs7QUFFWixVQUFLVixRQUFMLEdBQWdCLEtBQWhCO0FBQ0EsVUFBS0osUUFBTCxHQUFnQixJQUFJTCxRQUFKLE9BQWhCO0FBSFk7QUFJYjs7b0JBRURvQixPLHNCQUFVO0FBQ1IsU0FBS1gsUUFBTCxHQUFnQixJQUFoQjtBQUNBLFNBQUtKLFFBQUwsQ0FBY0YsS0FBZDtBQUNELEc7O29CQUVEa0IsUyx3QkFBWTtBQUNWLFNBQUtaLFFBQUwsR0FBZ0IsS0FBaEI7QUFDQSxTQUFLSixRQUFMLENBQWNGLEtBQWQ7QUFDQSxTQUFLbUIsa0JBQUwsQ0FBd0IsU0FBeEI7QUFDRCxHOzs7OztrQkFHWSxFQUFFSCxnQkFBRixFQUFXbkIsa0JBQVgsRUFBcUJDLFNBQVMsSUFBSWtCLE9BQUosRUFBOUIsRSIsImZpbGUiOiJob29rLmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgeyBFdmVudEVtaXR0ZXIgfSBmcm9tICdldmVudHMnO1xuXG5jbGFzcyBDb25zdW1lciB7XG5cbiAgY29uc3RydWN0b3IodHJhY2tlcikge1xuICAgIHRoaXMudHJhY2tlciA9IHRyYWNrZXI7XG4gICAgdGhpcy5jb25zdW1lcnMgPSBbXTtcbiAgfVxuXG4gIHJlc2V0KCkge1xuICAgIHRoaXMuY29uc3VtZXJzID0gW107XG4gIH1cblxuICB0cmFjayhjb25zdW1lciwgY2FsbGJhY2ssIHJlc29sdmUpIHtcbiAgICBsZXQgc3RlcDtcblxuICAgIGlmICh0aGlzLnRyYWNrZXIudHJhY2tpbmcpIHtcbiAgICAgIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby1wYXJhbS1yZWFzc2lnblxuICAgICAgY29uc3VtZXIucmVzcG9uc2UgPSBmdW5jdGlvbiByZXNwb25zZShyZXN1bHQpIHtcbiAgICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXBhcmFtLXJlYXNzaWduXG4gICAgICAgIGNvbnN1bWVyLnJlc3VsdCA9IHJlc3VsdDtcbiAgICAgICAgY2FsbGJhY2socmVzdWx0KTtcbiAgICAgICAgcmVzb2x2ZShyZXN1bHQpO1xuICAgICAgfTtcblxuICAgICAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXBhcmFtLXJlYXNzaWduXG4gICAgICBkZWxldGUgY29uc3VtZXIucmVzdWx0O1xuXG4gICAgICBzdGVwID0gdGhpcy5jb25zdW1lcnMucHVzaChjb25zdW1lcik7XG4gICAgICB0aGlzLnRyYWNrZXIuZW1pdCgnY29uc3VtZScsIGNvbnN1bWVyLCBzdGVwKTtcbiAgICB9XG4gIH1cblxuICBmaXJzdCgpIHtcbiAgICByZXR1cm4gdGhpcy5jb25zdW1lcnNbMF07XG4gIH1cblxuICBjb3VudCgpIHtcbiAgICByZXR1cm4gdGhpcy5jb25zdW1lcnMubGVuZ3RoO1xuICB9XG5cbiAgbGFzdCgpIHtcbiAgICByZXR1cm4gdGhpcy5jb25zdW1lcnNbdGhpcy5jb3VudCgpIC0gMV07XG4gIH1cblxuICBhdChzdGVwKSB7XG4gICAgcmV0dXJuIHRoaXMuY29uc3VtZXJzW3N0ZXAgLSAxXTtcbiAgfVxufVxuXG5jbGFzcyBUcmFja2VyIGV4dGVuZHMgRXZlbnRFbWl0dGVyIHtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuICAgIHRoaXMudHJhY2tpbmcgPSBmYWxzZTtcbiAgICB0aGlzLmNvbnN1bWVyID0gbmV3IENvbnN1bWVyKHRoaXMpO1xuICB9XG5cbiAgaW5zdGFsbCgpIHtcbiAgICB0aGlzLnRyYWNraW5nID0gdHJ1ZTtcbiAgICB0aGlzLmNvbnN1bWVyLnJlc2V0KCk7XG4gIH1cblxuICB1bmluc3RhbGwoKSB7XG4gICAgdGhpcy50cmFja2luZyA9IGZhbHNlO1xuICAgIHRoaXMuY29uc3VtZXIucmVzZXQoKTtcbiAgICB0aGlzLnJlbW92ZUFsbExpc3RlbmVycygnY29uc3VtZScpO1xuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IHsgVHJhY2tlciwgQ29uc3VtZXIsIHRyYWNrZXI6IG5ldyBUcmFja2VyKCkgfTtcbiJdfQ==