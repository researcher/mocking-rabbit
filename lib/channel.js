'use strict';

exports.__esModule = true;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _hook = require('./hook');

var _hook2 = _interopRequireDefault(_hook);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var tracker = _hook2.default.tracker;

var Channel = function () {
  function Channel() {
    (0, _classCallCheck3.default)(this, Channel);
  }

  // eslint-disable-next-line no-unused-vars
  Channel.assertExchange = function () {
    var _ref = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee(exhange, type, options) {
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              return _context.abrupt('return', true);

            case 1:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function assertExchange(_x, _x2, _x3) {
      return _ref.apply(this, arguments);
    }

    return assertExchange;
  }();

  // eslint-disable-next-line no-unused-vars


  Channel.assertQueue = function () {
    var _ref2 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee2(queue, options) {
      return _regenerator2.default.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              return _context2.abrupt('return', true);

            case 1:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    function assertQueue(_x4, _x5) {
      return _ref2.apply(this, arguments);
    }

    return assertQueue;
  }();

  // eslint-disable-next-line no-unused-vars


  Channel.bindQueue = function () {
    var _ref3 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee3(source, destination, routingKey) {
      return _regenerator2.default.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              return _context3.abrupt('return', true);

            case 1:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, this);
    }));

    function bindQueue(_x6, _x7, _x8) {
      return _ref3.apply(this, arguments);
    }

    return bindQueue;
  }();

  // eslint-disable-next-line no-unused-vars


  Channel.publish = function () {
    var _ref4 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee4(exchange, routingKey, content, options) {
      return _regenerator2.default.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              return _context4.abrupt('return', true);

            case 1:
            case 'end':
              return _context4.stop();
          }
        }
      }, _callee4, this);
    }));

    function publish(_x9, _x10, _x11, _x12) {
      return _ref4.apply(this, arguments);
    }

    return publish;
  }();

  // eslint-disable-next-line no-unused-vars


  Channel.sendToQueue = function () {
    var _ref5 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee5(queue, data) {
      return _regenerator2.default.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              return _context5.abrupt('return', true);

            case 1:
            case 'end':
              return _context5.stop();
          }
        }
      }, _callee5, this);
    }));

    function sendToQueue(_x13, _x14) {
      return _ref5.apply(this, arguments);
    }

    return sendToQueue;
  }();

  // eslint-disable-next-line no-unused-vars


  Channel.consume = function () {
    var _ref6 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee6(queue, callback) {
      return _regenerator2.default.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              return _context6.abrupt('return', new Promise(function (resolve) {
                tracker.consumer.track({ queue: queue }, callback, resolve);
              }));

            case 1:
            case 'end':
              return _context6.stop();
          }
        }
      }, _callee6, this);
    }));

    function consume(_x15, _x16) {
      return _ref6.apply(this, arguments);
    }

    return consume;
  }();

  Channel.ack = function () {
    var _ref7 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee7() {
      return _regenerator2.default.wrap(function _callee7$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              return _context7.abrupt('return', true);

            case 1:
            case 'end':
              return _context7.stop();
          }
        }
      }, _callee7, this);
    }));

    function ack() {
      return _ref7.apply(this, arguments);
    }

    return ack;
  }();

  Channel.close = function () {
    var _ref8 = (0, _asyncToGenerator3.default)(_regenerator2.default.mark(function _callee8() {
      return _regenerator2.default.wrap(function _callee8$(_context8) {
        while (1) {
          switch (_context8.prev = _context8.next) {
            case 0:
              return _context8.abrupt('return', true);

            case 1:
            case 'end':
              return _context8.stop();
          }
        }
      }, _callee8, this);
    }));

    function close() {
      return _ref8.apply(this, arguments);
    }

    return close;
  }();

  return Channel;
}();

exports.default = Channel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9jaGFubmVsLmpzIl0sIm5hbWVzIjpbInRyYWNrZXIiLCJDaGFubmVsIiwiYXNzZXJ0RXhjaGFuZ2UiLCJleGhhbmdlIiwidHlwZSIsIm9wdGlvbnMiLCJhc3NlcnRRdWV1ZSIsInF1ZXVlIiwiYmluZFF1ZXVlIiwic291cmNlIiwiZGVzdGluYXRpb24iLCJyb3V0aW5nS2V5IiwicHVibGlzaCIsImV4Y2hhbmdlIiwiY29udGVudCIsInNlbmRUb1F1ZXVlIiwiZGF0YSIsImNvbnN1bWUiLCJjYWxsYmFjayIsIlByb21pc2UiLCJyZXNvbHZlIiwiY29uc3VtZXIiLCJ0cmFjayIsImFjayIsImNsb3NlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBQ0E7Ozs7OztBQUVBLElBQU1BLFVBQVUsZUFBS0EsT0FBckI7O0lBRXFCQyxPOzs7OztBQUNuQjtVQUNhQyxjOzJGQUFlQyxPLEVBQVNDLEksRUFBTUMsTzs7Ozs7K0NBQ2xDLEk7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBR1Q7OztVQUNhQyxXOzZGQUFZQyxLLEVBQU9GLE87Ozs7O2dEQUN2QixJOzs7Ozs7Ozs7Ozs7Ozs7OztBQUdUOzs7VUFDYUcsUzs2RkFBVUMsTSxFQUFRQyxXLEVBQWFDLFU7Ozs7O2dEQUNuQyxJOzs7Ozs7Ozs7Ozs7Ozs7OztBQUdUOzs7VUFDYUMsTzs2RkFBUUMsUSxFQUFVRixVLEVBQVlHLE8sRUFBU1QsTzs7Ozs7Z0RBQzNDLEk7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBR1Q7OztVQUNhVSxXOzZGQUFZUixLLEVBQU9TLEk7Ozs7O2dEQUN2QixJOzs7Ozs7Ozs7Ozs7Ozs7OztBQUdUOzs7VUFDYUMsTzs2RkFBUVYsSyxFQUFPVyxROzs7OztnREFDbkIsSUFBSUMsT0FBSixDQUFZLFVBQUNDLE9BQUQsRUFBYTtBQUM5QnBCLHdCQUFRcUIsUUFBUixDQUFpQkMsS0FBakIsQ0FBdUIsRUFBRWYsWUFBRixFQUF2QixFQUFrQ1csUUFBbEMsRUFBNENFLE9BQTVDO0FBQ0QsZUFGTSxDOzs7Ozs7Ozs7Ozs7Ozs7OztVQUtJRyxHOzs7Ozs7Z0RBQ0osSTs7Ozs7Ozs7Ozs7Ozs7Ozs7VUFHSUMsSzs7Ozs7O2dEQUNKLEk7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O2tCQXRDVXZCLE8iLCJmaWxlIjoiY2hhbm5lbC5qcyIsInNvdXJjZXNDb250ZW50IjpbIlxuaW1wb3J0IGhvb2sgZnJvbSAnLi9ob29rJztcblxuY29uc3QgdHJhY2tlciA9IGhvb2sudHJhY2tlcjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQ2hhbm5lbCB7XG4gIC8vIGVzbGludC1kaXNhYmxlLW5leHQtbGluZSBuby11bnVzZWQtdmFyc1xuICBzdGF0aWMgYXN5bmMgYXNzZXJ0RXhjaGFuZ2UoZXhoYW5nZSwgdHlwZSwgb3B0aW9ucykge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXVudXNlZC12YXJzXG4gIHN0YXRpYyBhc3luYyBhc3NlcnRRdWV1ZShxdWV1ZSwgb3B0aW9ucykge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXVudXNlZC12YXJzXG4gIHN0YXRpYyBhc3luYyBiaW5kUXVldWUoc291cmNlLCBkZXN0aW5hdGlvbiwgcm91dGluZ0tleSkge1xuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgLy8gZXNsaW50LWRpc2FibGUtbmV4dC1saW5lIG5vLXVudXNlZC12YXJzXG4gIHN0YXRpYyBhc3luYyBwdWJsaXNoKGV4Y2hhbmdlLCByb3V0aW5nS2V5LCBjb250ZW50LCBvcHRpb25zKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdW51c2VkLXZhcnNcbiAgc3RhdGljIGFzeW5jIHNlbmRUb1F1ZXVlKHF1ZXVlLCBkYXRhKSB7XG4gICAgcmV0dXJuIHRydWU7XG4gIH1cblxuICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tdW51c2VkLXZhcnNcbiAgc3RhdGljIGFzeW5jIGNvbnN1bWUocXVldWUsIGNhbGxiYWNrKSB7XG4gICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiB7XG4gICAgICB0cmFja2VyLmNvbnN1bWVyLnRyYWNrKHsgcXVldWUgfSwgY2FsbGJhY2ssIHJlc29sdmUpO1xuICAgIH0pO1xuICB9XG5cbiAgc3RhdGljIGFzeW5jIGFjaygpIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIHN0YXRpYyBhc3luYyBjbG9zZSgpIHtcbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxufVxuIl19